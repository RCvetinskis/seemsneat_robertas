import React from "react";

const ErrorMsg = ({ error }) => {
  return <div className="error">{error}</div>;
};

export default ErrorMsg;
