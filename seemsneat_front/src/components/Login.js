import React, { useRef, useEffect, useState } from "react";
import { useContext } from "react";
import { UserContext } from "../context/mainContext";
import ErrorMsg from "./ErrorMsg";
import Greeting from "./Greeting";

const Login = () => {
  const { state, dispatch } = useContext(UserContext);
  const { error, user } = state;
  // validation states
  const [isEmaillValid, setEmailVlid] = useState(false);
  const [emailError, setEmailVlidError] = useState("");
  const [isPassValid, setPassValid] = useState(false);
  const [passError, setPassValidError] = useState("");
  // email regex
  const emailRegex =
    /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  const emailRef = useRef();
  const passwordRef = useRef();

  const loginHandler = async () => {
    if (isEmaillValid && isPassValid) {
      const refValues = {
        email: emailRef.current.value,
        password: passwordRef.current.value,
      };
      dispatch({ type: "FETCH_REQUEST" });
      try {
        const response = await fetch("/seemsneat-login", {
          method: "post",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(refValues),
        });
        const data = await response.json();

        if (data.error) {
          dispatch({ type: "FETCH_FAIL", payload: data.message });
        } else {
          // dispatches data to reducer
          dispatch({ type: "FETCH_SUCCESS", payload: data.data });
          // set user to localstorage
          localStorage.setItem("user", JSON.stringify(data.data));
        }
      } catch (err) {
        dispatch({ type: "FETCH_FAIL", payload: err });
      }
    }
  };
  function validateEmail() {
    const email = emailRef.current.value;
    if (emailRegex.test(email)) {
      setEmailVlid(true);
      setEmailVlidError("");
    } else {
      setEmailVlid(false);
      setEmailVlidError("Email is invalid");
    }
  }
  function validatePassword() {
    const password = passwordRef.current.value;
    if (password.length <= 0) {
      setPassValid(false);
      setPassValidError("Password required");
    } else {
      setPassValid(true);
      setEmailVlidError("");
    }
  }
  // checks if user priviously was logged in
  useEffect(() => {
    const loggedInUser = localStorage.getItem("user");
    if (loggedInUser) {
      dispatch({ type: "FETCH_SUCCESS", payload: loggedInUser });
    }
  }, [dispatch]);
  return (
    <div>
      {/* opens greeting  if user exists */}
      {user ? (
        <Greeting />
      ) : (
        <div className="login">
          <header className="login-header">
            <h1>Login</h1>
            <p>Please enter your username and your Password</p>
          </header>
          <div className="inputs-container">
            <div className="email-container">
              <i className="fa-regular fa-user"></i>
              <input
                ref={emailRef}
                type="email"
                placeholder="Username or E-mail"
                onChange={validateEmail}
              />
              {/* shows error message on wrong validation */}
              {isEmaillValid && error && error === "email not found" ? (
                <ErrorMsg error={error} />
              ) : isEmaillValid ? (
                <></>
              ) : (
                <ErrorMsg error={emailError} />
              )}
            </div>
            <div className="pass-container">
              {/* only this key was free from fontawesome */}
              <i className="fa-solid fa-key"></i>
              <input
                ref={passwordRef}
                type="password"
                placeholder="Password"
                onChange={validatePassword}
              />
              {/* shows error message on wrong validation */}
              {isPassValid && error && error === "wrong password" ? (
                <ErrorMsg error={error} />
              ) : isPassValid ? (
                <></>
              ) : (
                <ErrorMsg error={passError} />
              )}
            </div>
          </div>

          <button onClick={loginHandler}>Login</button>
        </div>
      )}
    </div>
  );
};

export default Login;
