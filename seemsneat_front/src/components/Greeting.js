import React from "react";
import { useSpring, animated } from "react-spring";
const Greeting = () => {
  const contentWrapper = useSpring({
    delay: 1000,
    from: { height: "0px" },
    to: { height: "350px" },
    config: { bounce: true },
  });

  return (
    <animated.div className="greeting" style={contentWrapper}>
      <h1>
        Welcome <span>To Seams Neat!</span>
      </h1>
    </animated.div>
  );
};

export default Greeting;
