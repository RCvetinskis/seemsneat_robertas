const express = require("express");
const router = express.Router();
const { login } = require("../controller/usersController");
router.post("/seemsneat-login", login);

module.exports = router;
