const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const seemsNeatSchema = new Schema({
  username: {
    type: String,
    required: false,
  },
  password: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
  },
});

module.exports = mongoose.model("seemsNeatSchema", seemsNeatSchema);
