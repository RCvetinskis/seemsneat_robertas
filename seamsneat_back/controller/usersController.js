const response = require("../module/response");
const userDb = require("../schema/seemsNeatSchema");
const bcrypt = require("bcrypt");

module.exports = {
  login: async (req, res) => {
    const { email, password } = req.body;
    const user = await userDb.findOne({ email });
    if (user) {
      const compare = await bcrypt.compare(password, user.password);
      if (compare) {
        // sends user to front if password is correct and user exists
        return response(res, "succesfully logged in", false, user);
      }
      return response(res, "wrong password", true);
    }
    return response(res, "email not found", true);
  },
};
