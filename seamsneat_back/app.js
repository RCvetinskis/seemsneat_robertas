const express = require("express");
const app = express();
const path = require("path");
const cors = require("cors");
const mongoose = require("mongoose");
const userRouter = require("./routes/userRouter");
const port = process.env.PORT || 4000;
require("dotenv").config();
mongoose
  .connect(process.env.MONGO_KEY)
  .then((res) => {
    console.log("CONNECTED");
  })
  .catch((e) => {
    console.log("ERROR");
  });

app.listen(port, (err) => {
  if (err) return console.log(err);
  console.log(`serve at http://localhost:${port}`);
});
app.use(express.json());
app.use(
  cors({
    origin: true,
    credentials: true,
    methods: "GET, POST",
  })
);
app.use("/", userRouter);

// serves frontend build folder
if(process.env.NODE_ENV === "production"){
  app.use(express.static(path.join(__dirname, "../seemsneat_front/build")));
  app.get("*", (req, res) =>
    res.sendFile(path.join(__dirname, "../seemsneat_front/build/index.html"))
  );
  
}
